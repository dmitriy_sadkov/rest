package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.model.Task;

import java.util.List;

public interface ITaskService {
    boolean persist(@Nullable final String taskName, @Nullable final String description, @Nullable final String projectId);

    void update(@Nullable final String id, @Nullable final String newName, @Nullable final String description);

    @NotNull
    List<Task> findAll();

    @NotNull List<Task> findAll(@NotNull String projectId);

    Task findOne(@NotNull final String taskId);

    void remove(@NotNull final String taskId);
}
