package ru.sadkov.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sadkov.tm.feign.ProjectFeign;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.service.IProjectService;

import java.util.List;

@Service
public class ProjectService extends AbstractService implements IProjectService {

    @NotNull
    @Autowired
    private ProjectFeign projectFeign;

    @Override
    public Project findOne(@NotNull final String projectId) {
        return projectFeign.getProjects(projectId);
    }

    @Override
    public void remove(@NotNull final String projectId) {
        projectFeign.deleteProject(projectId);
    }

    @Override
    public boolean persist(@Nullable String projectName, @Nullable String description) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (description == null || description.isEmpty()) return false;
        @NotNull final Project project = new Project(projectName, description);
        projectFeign.createProject(project);
        return true;
    }

    public void update(@Nullable final String id, @Nullable final String newName, @Nullable final String description) {
        if (id == null || newName == null || id.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        @NotNull final Project project = findOne(id);
        project.setName(newName);
        project.setDescription(description);
        projectFeign.updateProject(project);
    }


    @Override
    public @NotNull List<Project> findAll() {
        return projectFeign.projects();
    }
}
