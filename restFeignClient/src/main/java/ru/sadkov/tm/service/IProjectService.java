package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    boolean persist(@Nullable final String projectName, @Nullable final String description);

    void update(@Nullable final String id, @Nullable final String newName, @Nullable final String description);

    @NotNull
    List<Project> findAll();

    Project findOne(@NotNull final String projectId);

    void remove(@NotNull final String projectId);
}
