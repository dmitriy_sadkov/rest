package ru.sadkov.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sadkov.tm.feign.TaskFeign;
import ru.sadkov.tm.model.Task;
import ru.sadkov.tm.service.ITaskService;

import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Autowired
    @NotNull
    private TaskFeign taskFeign;

    @Override
    public boolean persist(@Nullable String taskName, @Nullable String description, @Nullable String projectId) {
        if (taskName == null || taskName.isEmpty()) return false;
        if (description == null || description.isEmpty()) return false;
        if (projectId == null || projectId.isEmpty()) return false;
        @NotNull final Task task = new Task(taskName, description, projectId);
        taskFeign.createTask(task);
        return true;
    }

    @Override
    public void update(@Nullable String id, @Nullable String newName, @Nullable String description) {
        if (id == null || newName == null || id.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        @NotNull final Task task = findOne(id);
        task.setName(newName);
        task.setDescription(description);
        taskFeign.updateTask(id,task);
    }

    @Override
    public @NotNull List<Task> findAll(@NotNull final String projectId) {
        return taskFeign.tasks(projectId);
    }

    @Override
    public Task findOne(@NotNull String taskId) {
        return taskFeign.getTask(taskId);
    }

    @Override
    public void remove(@NotNull String taskId) {
        taskFeign.deleteTask(taskId);
    }

    @Override
    public @NotNull List<Task> findAll() {
        return taskFeign.allTasks();
    }
}
