package ru.sadkov.tm.feign;

import org.jetbrains.annotations.NotNull;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.sadkov.tm.model.Project;

import java.util.List;

@FeignClient(url = "http://localhost:8080/spring-mvc/projects", name = "projectFeign")
public interface ProjectFeign {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    List<Project> projects();

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    Project getProjects(@PathVariable(name = "id") @NotNull final String id);

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_XML_VALUE)
    void createProject(@RequestBody @NotNull final Project projectDTO);

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    void updateProject(@RequestBody @NotNull final Project projectDTO);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    void deleteProject(@PathVariable(name = "id") @NotNull final String id);

}
