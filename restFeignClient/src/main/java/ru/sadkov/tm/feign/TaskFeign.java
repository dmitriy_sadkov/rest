package ru.sadkov.tm.feign;

import org.jetbrains.annotations.NotNull;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.sadkov.tm.model.Task;

import java.util.List;

@FeignClient(url = "http://localhost:8080/spring-mvc/tasks", name = "taskFeign")
public interface TaskFeign {

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    List<Task> tasks(@PathVariable(name = "id") @NotNull final String projectId);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    List<Task> allTasks();

    @RequestMapping(value = "/task/{id}", method = RequestMethod.GET)
    Task getTask(@PathVariable(name = "id") @NotNull final String id);

    @RequestMapping(value = "/", method = RequestMethod.POST)
    Task createTask(@RequestBody @NotNull final Task taskDTO);


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    void updateTask(@PathVariable(name = "id") @NotNull final String id,
                    @RequestBody @NotNull final Task taskDTO);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    void deleteTask(@PathVariable(name = "id") @NotNull final String id);
}
