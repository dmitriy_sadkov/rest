package ru.sadkov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.model.enumerate.Status;
import ru.sadkov.tm.util.RandomUtil;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Task {
    @NotNull
    String id;

    @NotNull
    String name;

    @Nullable
    String description;

    @NotNull
    String projectId;

    @NotNull
    Date dateCreate;

    @Nullable
    Date dateBegin;

    @Nullable
    Date dateEnd;

    @NotNull
    Status status;

    public Task(@NotNull String name, @Nullable String description, @NotNull String projectId) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.id = RandomUtil.UUID();
        this.dateCreate = new Date();
        this.dateBegin = new Date();
        this.dateEnd = new Date();
        this.status = Status.PLANNED;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", projectId='" + projectId + '\'' +
                ", dateCreate=" + dateCreate +
                ", dateBegin=" + dateBegin +
                ", dateEnd=" + dateEnd +
                ", status=" + status +
                '}';
    }
}
