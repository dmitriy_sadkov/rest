package ru.sadkov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.model.enumerate.Status;
import ru.sadkov.tm.util.RandomUtil;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
public class Project {

    @NotNull
    String id;

    @NotNull
    String name;

    @Nullable
    String description;

    @NotNull
    String userId = "bcae7246-620b-4b41-bb0c-8fc9be2cee7c";

    @NotNull
    Date dateCreate;

    @Nullable
    Date dateBegin;

    @Nullable
    Date dateEnd;

    @NotNull
    Status status;

    public Project(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
        dateCreate = new Date();
        dateBegin = new Date();
        dateEnd = new Date();
        status = Status.PLANNED;
        id = RandomUtil.UUID();
    }
}
