package ru.sadkov.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.sadkov.tm.model.Task;
import ru.sadkov.tm.service.ITaskService;

import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Autowired
    @NotNull RestTemplate restTemplate;

    @Override
    public boolean persist(@Nullable String taskName, @Nullable String description, @Nullable String projectId) {
        if (taskName == null || taskName.isEmpty()) return false;
        if (description == null || description.isEmpty()) return false;
        if (projectId == null || projectId.isEmpty()) return false;
        Task task = new Task(taskName, description, projectId);
        task = restTemplate.postForObject("http://localhost:8080/spring-mvc/tasks", task, Task.class);
        return true;
    }

    @Override
    public void update(@Nullable String id, @Nullable String newName, @Nullable String description) {
        if (id == null || newName == null || id.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        Task task = findOne(id);
        task.setName(newName);
        task.setDescription(description);
        restTemplate.put("http://localhost:8080/spring-mvc/tasks/{id}", task, id);
    }

    @Override
    public @NotNull List<Task> findAll(@NotNull final String projectId) {
        ResponseEntity<List<Task>> response = restTemplate.exchange(
                "http://localhost:8080/spring-mvc/tasks/{id}",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Task>>() {
                },
                projectId);
        return response.getBody();
    }

    @Override
    public Task findOne(@NotNull String taskId) {
        return restTemplate.getForObject("http://localhost:8080/spring-mvc/tasks/task/{id}", Task.class, taskId);
    }

    @Override
    public void remove(@NotNull String taskId) {
        restTemplate.delete("http://localhost:8080/spring-mvc/task/{id}", taskId);
    }

    @Override
    public @NotNull List<Task> findAll() {
        ResponseEntity<List<Task>> response = restTemplate.exchange(
                "http://localhost:8080/spring-mvc/tasks",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Task>>() {
                });
        return response.getBody();
    }
}
