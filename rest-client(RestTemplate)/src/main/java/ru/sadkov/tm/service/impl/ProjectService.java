package ru.sadkov.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.service.IProjectService;

import java.util.List;

@Service
public class ProjectService extends AbstractService implements IProjectService {

    @NotNull
    @Autowired
    private RestTemplate restTemplate;


    @Override
    public Project findOne(@NotNull final String projectId) {
        Project project = restTemplate.getForObject("http://localhost:8080/spring-mvc/projects/{id}", Project.class, projectId);
        return project;
    }

    @Override
    public void remove(@NotNull final String projectId) {
        restTemplate.delete("http://localhost:8080/spring-mvc/projects/{id}",projectId);
    }

    @Override
    public boolean persist(@Nullable String projectName, @Nullable String description) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (description == null || description.isEmpty()) return false;
        Project project = new Project(projectName, description);
        project = restTemplate.postForObject("http://localhost:8080/spring-mvc/projects",project,Project.class);
        return true;
    }

    @Transactional
    public void update(@Nullable final String id, @Nullable final String newName, @Nullable final String description) {
        if (id == null || newName == null || id.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        Project project = findOne(id);
        project.setName(newName);
        project.setDescription(description);
        restTemplate.put("http://localhost:8080/spring-mvc/projects/{id}",project,id);
    }


    @Override
    public @NotNull List<Project> findAll() {
        ResponseEntity<List<Project>> response = restTemplate.exchange(
                "http://localhost:8080/spring-mvc/projects",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Project>>() {
                });
        return response.getBody();
    }
}
