# TM-SP-02
```
Task tm-sp-02 I-TECO
```
## TECHNOLOGIES

* Maven 4.0
* Java SE 1.8
* Junit 4.11
* Lombok 1.18.10
* Hibernate
* Spring 5
* Spring MVC
* Spring Data JPA
* Spring Security
* JSP
* Bootstrap
* MySQL
* PostgreSQL

## DEVELOPER
```
Sadkov Dmitriy
dmitriy_sadkov@mail.ru
```
## BUILDING FROM SOURCE
```
-mvn clean install
```
## SOFTWARE REQUIREMENTS
```
-jdk 1.8
```

## USING THE PROJECT MANAGER
```
Using tomcat plugin
```
