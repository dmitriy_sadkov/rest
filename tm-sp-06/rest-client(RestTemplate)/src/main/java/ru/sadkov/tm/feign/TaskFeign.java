package ru.sadkov.tm.feign;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.sadkov.tm.model.Task;

import java.util.List;

//@FeignClient(url = "http://localhost:8080/spring-mvc/tasks", name = "taskFeign")
//@Component
public interface TaskFeign {

    static TaskFeign client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder().contract(new SpringMvcContract()).encoder(new SpringEncoder(objectFactory)).decoder(new SpringDecoder(objectFactory)).target(TaskFeign.class, baseUrl);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    List<Task> tasks(@PathVariable(name = "id") @NotNull final String projectId);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    List<Task> allTasks();

    @RequestMapping(value = "/{projectId}/{id}", method = RequestMethod.GET)
    Task getTask(@PathVariable(name = "projectId") @NotNull final String projectId,
                 @PathVariable(name = "id") @NotNull final String id);

    @RequestMapping(value = "/", method = RequestMethod.POST)
    Task createTask(@RequestBody @NotNull final Task taskDTO);

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    void updateTask(@PathVariable(name = "id") @NotNull final String id,
                    @RequestBody @NotNull final Task taskDTO);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    void deleteTask(@PathVariable(name = "id") @NotNull final String id);
}
