package ru.sadkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.Task;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.service.ITaskService;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    ITaskService taskService;

    @NotNull
    @Autowired
    IProjectService projectService;

    @RequestMapping(value = "/spring-mvc/tasks/{id}", method = RequestMethod.GET)
    public String allTasks(Model model, @PathVariable(name = "id") @NotNull final String projectId) {
        model.addAttribute("tasksList", taskService.findAll(projectId));
        model.addAttribute("projectId", projectId);
        return "tasks";
    }

    @RequestMapping(value = "/spring-mvc/tasks/task/{id}", method = RequestMethod.GET)
    public String getTask(Model model, @PathVariable(name = "id") @NotNull final String taskId) {
        Task task = taskService.findOne(taskId);
        System.out.println(task);
        model.addAttribute("task", task);
        return "task";
    }

    @RequestMapping(value = "/spring-mvc/task-create/{id}", method = RequestMethod.GET)
    public String createTaskGet(Model model, @PathVariable(name = "id") @NotNull final String projectId) {
        model.addAttribute("projectId", projectId);
        return "createTask";
    }


    @RequestMapping(value = "/spring-mvc/task-create/{id}", method = RequestMethod.POST)
    public String createTaskPost(@PathVariable(name = "id") @NotNull final String projectId,
                                 @ModelAttribute("name") final String name,
                                 @ModelAttribute("description") final String description, BindingResult result) {
        taskService.persist(name, description, projectId);
        return "redirect:/spring-mvc/tasks/" + projectId;
    }

    @RequestMapping(value = "/spring-mvc/editTask/{id}", method = RequestMethod.GET)
    public String editTask(Model model, @PathVariable("id") String id) {
        Task task = taskService.findOne(id);
        model.addAttribute("projectId", task.getProjectId());
        model.addAttribute("task", task);
        return "editTask";
    }

    @RequestMapping(value = "/spring-mvc/editTask/{id}", method = RequestMethod.POST)
    public String editTaskPost(@PathVariable(name = "id") @NotNull final String projectId,
                                  @ModelAttribute("taskId") @NotNull final String taskId,
                                  @ModelAttribute("name") final String name,
                                  @ModelAttribute("description") final String description) {
        taskService.update(taskId,name,description);
        return "redirect:/spring-mvc/tasks/" + projectId;
    }

    @RequestMapping(value = "/spring-mvc/deleteTask/{id}", method = RequestMethod.GET)
    public String deleteTask(@PathVariable(name = "id") @NotNull final String taskId){
        String projectId = taskService.findOne(taskId).getProjectId();
        taskService.remove(taskId);
        return "redirect:/spring-mvc/tasks/" + projectId;
    }

}
