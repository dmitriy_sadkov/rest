package ru.sadkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.service.IProjectService;

import java.util.List;

@Controller
public class ProjectController {

//    @NotNull
//    @Autowired
//    ITaskService taskService;

    @NotNull
    @Autowired
    IProjectService projectService;

    @GetMapping("/")
    public String projects(@NotNull Model model) {
        System.out.println(projectService);
        @NotNull List<Project> projects = projectService.findAll();
        model.addAttribute("projectList", projects);
        return "projects";
    }

    @RequestMapping(value = "/spring-mvc/project-create", method = RequestMethod.GET)
    public String createProjectGet() {
        return "projectCreate";
    }


    @RequestMapping(value = "/spring-mvc/project-create", method = RequestMethod.POST)
    public String createProjectPost(@ModelAttribute("name") final String name, @ModelAttribute("description") final String description, BindingResult result) {
        projectService.persist(name, description);
        return "redirect:/";
    }

    @RequestMapping(value = "/spring-mvc/edit/{id}", method = RequestMethod.GET)
    public String editProject(Model model, @PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        model.addAttribute("project", project);
        return "editProject";
    }

    @RequestMapping(value = "/spring-mvc/edit", method = RequestMethod.POST)
    public String editProjectPost(@ModelAttribute("project") Project project) {
        @NotNull final Project fullProject = projectService.findOne(project.getId());
        projectService.update(project.getId(), project.getName(), project.getDescription());

        return "redirect:/";
    }

    @RequestMapping(value = "/spring-mvc/view/{id}", method = RequestMethod.GET)
    public String viewProject(Model model, @PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        model.addAttribute("project", project);
        return "project";
    }

    @RequestMapping(value = "/spring-mvc/delete/{id}", method = RequestMethod.GET)
    public String removeProject(@PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        projectService.remove(id);
        return "redirect:/";
    }

    @RequestMapping(value = "/home")
    public String backToProjectList(Model model) {
        return "redirect:/projects";
    }

    @RequestMapping(value = "/projects/{login}", method = RequestMethod.GET)
    public String projectsForUser(Model model, @PathVariable("login") String login) {
        @NotNull List<Project> projects = projectService.findAll();
        model.addAttribute("projectList", projects);
        return "projects";
    }

}
