package ru.sadkov.tm.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.sadkov.tm.model.Task;
import ru.sadkov.tm.model.enumerate.Status;

import java.util.Date;
import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task, String> {

    Task findOneByName(String taskName);

    boolean existsByNameAndProjectId(String taskName, String projectId);

    void deleteByName(String taskName);

//    List<Task> findAll();

    @Modifying
    @Query("update Task set name = :name, description = :description where id = :id")
    void update(@Param("id") String id, @Param("name") String newName, @Param("description") String description);

    @Query("select t from Task t where t.name like %:part% or t.description like %:part%")
    List<Task> getTasksByPart(@Param("part") String part);

    List<Task> findAllByStatus(Status status);

    @Modifying
    @Query("update Task set status = :status, dateBegin = :date where name = :name")
    void startTask(@Param("status") Status process, @Param("name") String taskName, @Param("date") Date startDate);

    @Modifying
    @Query("update Task set status = :status, dateEnd = :date where name = :name")
    void endTask(@Param("status") Status done, @Param("name") String taskName, @Param("date") Date endDate);

    List<Task> findAllByProjectId(String id);
}
