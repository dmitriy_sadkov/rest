package ru.sadkov.tm.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sadkov.tm.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    User findByLogin(String login);

    boolean existsByLogin(String login);
}
