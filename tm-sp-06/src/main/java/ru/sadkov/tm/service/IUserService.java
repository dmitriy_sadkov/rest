package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.model.User;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface IUserService {

    void userRegister(@Nullable final String login, @Nullable final String password);

    boolean login(@Nullable final String login, @Nullable final String password);

    @Nullable
    User findOneByLogin(@Nullable final String login);

    void addTestUsers() throws NoSuchAlgorithmException;

    @NotNull
    List<User> findAll();

    void clear();

    void userRegister(@Nullable User user);

    void userAddFromData(@Nullable final User user);

    void load(@Nullable final List<User> users);

    @Nullable
    User findOneById(@NotNull final String userId);

    @NotNull
    User createUser(String login, String password);
}
